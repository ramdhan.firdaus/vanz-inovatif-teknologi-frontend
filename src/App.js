import { BrowserRouter as Router, Route, Routes } from 'react-router-dom'
import HomePageComponent from './components/HomePageComponent';
import Navbar from './components/Navbar';
import Footer from './components/Footer';
import MyOrderComponent from './components/MyOrderComponent';
import ProductComponent from './components/ProductComponent';
import MyStoreComponent from './components/MyOrderComponent';

function App() {
  return (<>
    <Navbar />
    <Router>
      <div className="">
        <Routes>
          <Route path="/" element={<HomePageComponent />}></Route>
          <Route path="/my-store" element={<MyStoreComponent />}></Route>
          <Route path="/product" element={<ProductComponent />}></Route>
          <Route path="/my-order" element={<MyOrderComponent />}></Route>
        </Routes>
      </div>
    </Router>
    <Footer />
  </>
  );
}

export default App;
