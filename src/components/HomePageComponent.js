import React, { useState, useEffect } from 'react'
import product1 from '../assets/image/product1.jpg'
import product2 from '../assets/image/product2.jpg'
import product3 from '../assets/image/product3.jpg'
import product4 from '../assets/image/product4.jpg'
import { Icon } from '@iconify/react';
import url from '../config/url'
import axios from 'axios'

const HomePageComponent = () => {
    const [product, setProduct] = useState([])
    const [store, setStore] = useState([])

    useEffect(() => {
        axios.get(`${url}/product`).then((res) => {
            setProduct(res.data.product)
        })
        axios.get(`${url}/store`).then((res) => {
            setStore(res.data.store)
        })
    }, []);

    return (<>
        <section id="hero" className="d-flex align-items-center">
            <div className="container" data-aos="zoom-out" data-aos-delay="100">
                <div className="row">
                    <div className="col-xl-6">
                        <h1>OIL-Cuan</h1>
                        <h2>Making money from oil waste</h2>
                    </div>
                </div>
            </div>
        </section>

        <section id="counts" className="counts">
            <div className="container">
                <div className="row">
                    <div className="col-lg-6 col-md-6 mt-5 mt-md-0">
                        <div className="count-box">
                            <i className="bi bi-emoji-smile"></i>
                            <span data-purecounter-start="0" data-purecounter-end="521" data-purecounter-duration="1" className="purecounter">{product.length}</span>
                            <p>Total OIL-Cuan Product</p>
                        </div>
                    </div>

                    <div className="col-lg-6 col-md-6 mt-5 mt-md-0">
                        <div className="count-box">
                            <i className="bi bi-calendar2-week"></i>
                            <span data-purecounter-start="0" data-purecounter-end="521" data-purecounter-duration="1" className="purecounter">{store.length}</span>
                            <p>Total Store</p>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <section id="products" className="products section-bg">
            <div className="container" data-aos="fade-up">

                <div className="section-title">
                    <h2>Produk</h2>
                </div>

                <div className="row">

                    <div className="col-lg-3 col-md-6 d-flex align-items-stretch">
                        <div className="product">
                            <div className="product-img">
                                <img src={product1} className="img-fluid" alt="" />
                            </div>
                            <div className="product-info">
                                <h4>Lilin</h4>
                            </div>
                        </div>
                    </div>

                    <div className="col-lg-3 col-md-6 d-flex align-items-stretch">
                        <div className="product">
                            <div className="product-img">
                                <img src={product2} className="img-fluid" alt="" />
                            </div>
                            <div className="product-info">
                                <h4>Sabun</h4>
                            </div>
                        </div>
                    </div>

                    <div className="col-lg-3 col-md-6 d-flex align-items-stretch">
                        <div className="product">
                            <div className="product-img">
                                <img src={product3} className="img-fluid" alt="" />
                            </div>
                            <div className="product-info">
                                <h4>Aromaterapi</h4>
                            </div>
                        </div>
                    </div>

                    <div className="col-lg-3 col-md-6 d-flex align-items-stretch">
                        <div className="product">
                            <div className="product-img">
                                <img src={product4} className="img-fluid" alt="" />
                            </div>
                            <div className="product-info">
                                <h4>Deterjen</h4>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <section id="contact" className="contact">
            <div className="container" data-aos="fade-up">

                <div className="section-title">
                    <h2>Kontak</h2>
                </div>

                <div className="row" data-aos="fade-up" data-aos-delay="100">

                    <div className="col-lg-6">

                        <div className="row">
                            <div className="col-md-12">
                                <div className="info-box">
                                    <Icon icon="ant-design:home-filled" />
                                    <h3>Alamat</h3>
                                    <p>Jl. Ketilang IV D VII, Kelurahan Larangan, Kecamatan Harjamukti, Kota Cirebon</p>
                                </div>
                            </div>

                        </div>

                    </div>

                    <div className="col-lg-6">
                        <div className="row">
                            <div className="col-md-6">
                                <div className="info-box">
                                    <Icon icon="clarity:email-solid" />
                                    <h3>Email</h3>
                                    <a href='https://mail.google.com/mail/?view=cm&fs=1&to=ramdhanfirdaus33@gmail.com'>ramdhanfirdaus33@gmail.com</a>
                                </div>
                            </div>
                            <div className="col-md-6">
                                <div className="info-box">
                                    <Icon icon="bxs:phone-call" />
                                    <h3>No. Telepon</h3>
                                    <p>0882-1817-3671</p>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>

            </div>
        </section>
    </>
    )
}

export default HomePageComponent