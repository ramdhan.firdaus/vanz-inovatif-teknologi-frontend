import React from 'react'
import { Icon } from '@iconify/react';

const Footer = () => {
    return (
        <footer id="footer">
            <div className="container d-md-flex py-4">
                <div className="me-md-auto text-center text-md-start">
                    <div className="copyright">
                        &copy; Copyright <strong><span>Ramdhan Firdaus Amelia</span></strong>. All Rights Reserved
                    </div>
                    <div className="credits">
                        Designed by r_adam29
                    </div>
                </div>
                <div className="social-links text-center text-md-end pt-3 pt-md-0">
                    <a href="https://github.com/ramdhanfirdaus" className="twitter"><Icon icon="akar-icons:github-fill" /></a>
                    <a href="https://gitlab.com/ramdhanfirdaus" className="facebook"><Icon icon="ant-design:gitlab-outlined" /></a>
                    <a href="https://www.instagram.com/r_adam29" className="instagram"><Icon icon="akar-icons:instagram-fill" /></a>
                    <a href="https://www.linkedin.com/in/ramdhan-firdaus-amelia/" className="linkedin"><Icon icon="akar-icons:linkedin-box-fill" /></a>
                </div>
            </div>
        </footer>
    )
}

export default Footer