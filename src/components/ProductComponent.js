import React from 'react'

const ProductComponent = () => {
    return (
        <section id="hero" className="d-flex align-items-center">
            <div className="container" data-aos="zoom-out" data-aos-delay="100">
                <div className="row">
                    <div className="col-xl-6">
                        <h1>Product Component</h1>
                    </div>
                </div>
            </div>
        </section>
    )
}

export default ProductComponent