import React from 'react'

const Navbar = () => {
    return (
        <header id="header" className="fixed-top d-flex align-items-center border-bottom border-success">
            <div className="container d-flex align-items-center">
                <h1 className="logo me-auto"><a className="nav-link scrollto" href="./">Oil-Cuan</a></h1>
                <nav id="navbar" className="navbar order-last order-lg-0">
                    <ul>
                        <li><a className="nav-link scrollto" href="/">Home</a></li>
                        <li><a className="nav-link scrollto" href="/my-store">My Store</a></li>
                        <li className="dropdown"><a className="nav-link scrollto" href="/product"><span>Buy Oil-Cuan</span> <i className="bi bi-chevron-down"></i></a>
                            <ul>
                                <li><a className="nav-link scrollto" href="/product">Product Oil-Cuan</a></li>
                                <li><a className="nav-link scrollto" href="/my-order">Your Order</a></li>
                            </ul>
                        </li>
                    </ul>
                </nav>
            </div>
        </header>
    )
}

export default Navbar